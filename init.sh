#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

cnl="$1"
seck="$2"

if [ ${#cnl} -lt 2 ]; then
    exit
fi
if [ ${#seck} -lt 5 ]; then
    exit
fi
echo "CNL SECK"

# installs
apt-get update -y
#apt-get upgrade -yq
python /usr/bin/python3
apt-get install -y lrzsz nload iftop build-essential module-assistant python3-dev python3-setuptools unzip makepasswd cron ipset irqbalance libsodium-dev
ufw disable

if grep -q 'Debian GNU/Linux 10 (buster)' /etc/os-release
then
    echo iptables-persistent iptables-persistent/autosave_v4 boolean true | debconf-set-selections
    echo iptables-persistent iptables-persistent/autosave_v6 boolean true | debconf-set-selections
    apt-get install -y  iptables-persistent
else
    apt-get install -y  xtables-addons-common
fi


#module-assistant auto-install xtables-addons-source
#depmod -a
APTDONE=$?
echo "###### APTDONE $APTDONE"
if [ "$APTDONE" -ne 0 ];   then exit;  fi
#if grep -q 'Debian GNU/Linux 10 (buster)' /etc/os-release
#then
#    apt-get install -y python3-pip
#    #curl  https://bootstrap.pypa.io/pip/3.5/get-pip.py -o get-pip.py
#else
#    curl  https://bootstrap.pypa.io/pip/3.5/get-pip.py -o get-pip.py
#fi
#python3 get-pip.py
apt-get install -y python3-requests python3-psutil
PIPDONE=$?
echo "###### PIPDONE $PIPDONE"
if [ "$PIPDONE" -ne 0 ];   then exit;  fi

# make ssh
mkdir /root/.ssh
touch /root/.ssh/authorized_keys
if ! grep -q "root@jumper" /root/.ssh/authorized_keys
then
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCrkmA4sGE7g0b5QSk9goaLpzTyVBSbkDxADajSbisNb++m0AszZKzBHIMNPAhNq+dHnpmYs526j10PSyb+1Y1DAbWHVcaBIRiZrYELLkYDwFuLCCPmfTdo7pogBDdsdh6v29adsZCxAUgUmKIPBPG3IZZbhdc9YE3zY4erxmfLC+eemyoZ6RgXB4gpLNjrdWXwQbcPMBD79GAKVNcDanfb3xEhZsBIsV+UziosWimGFhtt44HWcnG2PgAg0RC6S+23G4BoS1tD1giRRZml+PkAIO3+rH1b+hII4E4JIyBGfg/OSC4IwyOb1nBrMHIxbsr+6NFNd/0HjtBe84hxRmvT root@jumper" >> /root/.ssh/authorized_keys
    SSHDONE=$?
    echo "###### SSHDONE $SSHDONE";
    if [ "$SSHDONE" -ne 0 ];   then exit;  fi
fi

# ssserver
pwdtmpl=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 16)
workersto=$(nproc)
ssconfile="/etc/ss.conf"
# move file

cat > /etc/ss.conf << ENDOF_SSCONF
{
 "local_port":1080,
 "local_address": "127.0.0.1",
 "port_password": {
    "80": "$pwdtmpl"
 },
 "timeout":60,
 "method":"aes-256-gcm",
 "fast_open": true,
 "workers": "$workersto"
}
ENDOF_SSCONF
SSCDONE=$?
echo "###### SSCDONE $SSCDONE"
if [ "$SSCDONE" -ne 0 ];   then exit;  fi


# bashrc ll
cat >> /root/.bashrc << ENDOF_BASHRC

export LS_OPTIONS='--color=auto'
alias ll='ls \$LS_OPTIONS -l'
ENDOF_BASHRC

# sed and change sshd config
sed -i 's/^PasswordAuthentication.*//' /etc/ssh/sshd_config
sed -i 's/^PermitEmptyPasswords.*//' /etc/ssh/sshd_config
sed -i 's/^PermitRootLogin.*//' /etc/ssh/sshd_config

cat >> /etc/ssh/sshd_config << ENDOF_SSHCONF

PasswordAuthentication no
PermitEmptyPasswords no
PermitRootLogin without-password
ENDOF_SSHCONF


#部署前，先卸载v2ray
cd /root
wget https://gitlab.com/phyxis1/test/-/raw/main/uninstall.sh
chmod +x uninstall.sh
bash /root/uninstall.sh

#部署v2ray
wget https://gitlab.com/phyxis1/test/-/raw/main/install-release-prod.sh
GET_V2RAY=$?
echo "###### GET_V2RAY $GET_V2RAY"
if [ "$GET_V2RAY" -ne 0 ];   then exit;  fi

chmod +x install-release-prod.sh
bash /root/install-release-prod.sh
rm -rf /root/install-release-prod.sh
sed -i "s/oJcWVfcScq3NNgbD/$pwdtmpl/g" /usr/local/etc/v2ray/config.json

systemctl enable v2ray
systemctl start v2ray


# Add monitor
rm -rf /root/server_status_check.py
wget https://gitlab.com/phyxis1/test/-/raw/main/server_status_check.py
cp /root/server_status_check.py /etc/server_status_check.py
if ! grep -q "server_status_check" /etc/crontab
then
    echo "*/1 * * * * root /usr/bin/python3 /etc/server_status_check.py -c $cnl -s $seck >> /root/report.log 2>&1" >> /etc/crontab;
    MONDONE=$?
    echo "###### MONDONE $MONDONE";
    if [ "$MONDONE" -ne 0 ];   then exit;  fi
fi


echo "###### CONGRATULATIONS"

