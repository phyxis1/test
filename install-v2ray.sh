#!/bin/bash

deploy_func () {
    echo ---------------------------------------------------------------------
    install="wget https://gitlab.com/phyxis1/test/-/raw/main/init.sh && bash /root/init.sh $1 $2 && sleep 1 && rm -rf /root/init.sh && sleep 1"
    echo $4
    /usr/bin/ssh -o ConnectTimeout=15 -t $4 "${install}"
    if [ "$?" -ne 0 ];   then
        echo "--------------------------------------------------------------------------------------------" "$3"
    fi
}

ssh-keyscan -H $9 >> ~/.ssh/known_hosts

pkg="common"
if [ -n "${10}" ]
then
  pkg="${10}"
fi
# mysql -uapiuser -h 10.43.64.3 -pht69PCkTLddjbxKZ -e "INSERT INTO rocket.tb_record (ugid, isp, zone, ip, cap_net, cap_ips, cap_load, rank, port, server) VALUES ('$5', '$1', '$6', '$9', $4, $3, 0.60, $7, 80, 1)"
mysql -uapiuser -h 10.43.64.3 -pht69PCkTLddjbxKZ -e "INSERT INTO rocket.tb_record (ugid, isp, zone, ip, cap_net, cap_ips, cap_load, \`rank\`, \`port\`, \`server\`, pkg) VALUES ('$5', '$1', '$6', '$9', $4, $3, 0.60, $7, 80, 1, '$pkg')"

deploy_func "$1" "$2" "$8" "$9"
# install do xxxpassword cap_ips cap_net ugid zone rank vip ip pkg
#          1     2          3    4       5     6    7    8  9   10

# example:
# ./install-v2ray.sh do xaa2Ohtoow 200  25 in          10 10 0 159.65.155.186
#  do the installation with DigitalOcean Server( do ) limited with 200 max user at a same time, avg 25 mbps in+out bandwith, India Server( in ), at zone 10, rank 10, no vip server, with ip 159xxxxx
