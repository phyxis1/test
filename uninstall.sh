#!/bin/bash
echo "正在停止 v2ray 服务..."
sudo systemctl stop v2ray

echo "正在禁用 v2ray 服务..."
sudo systemctl disable v2ray

echo "正在删除 v2ray 文件..."
sudo rm -f /usr/local/bin/v2ctl /usr/local/bin/v2ray

echo "正在删除 v2ray 服务文件..."
sudo rm -f /etc/systemd/system/v2ray.service
sudo rm -f /etc/systemd/system/v2ray@.service

echo "正在删除geo目录..."
sudo rm -rf /usr/local/share/

echo "正在删除 v2ray 配置目录..."
sudo rm -rf /usr/local/etc/v2ray/

echo "正在重新加载系统服务..."
sudo systemctl daemon-reload

echo "已经成功卸载 v2ray！"
