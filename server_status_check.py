#!/usr/bin/python
# -*- coding: utf-8 -*-
""" Python reporter
"""
__date__ = '2023-3'
__license__ = 'PHXYIS'

import os
import time
import hmac
import json
import psutil
import random
import hashlib
import requests
import datetime
import argparse
import subprocess
import multiprocessing
import ipaddress


def is_v2ray_inAlive():
    try:
        # 尝试获取v2ray服务的状态
        process = subprocess.run(['systemctl', 'is-active', 'v2ray'], check=False, stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
        # 需要服务是active 状态
        return 'inactive' in process.stdout.decode()
    except Exception as e:
        # 如果执行命令时出现异常，返回False
        print(f"An error occurred: {e}")
        return False


def tail(f, n):
    p = subprocess.Popen(['tail', '-n', str(n), f], stdout=subprocess.PIPE)
    soutput, sinput = p.communicate()
    return soutput


def get_uptime():
    uptime_seconds = 0
    with open('/proc/uptime', 'r') as f:
        uptime_seconds = int(float(f.readline().split()[0]))
    return uptime_seconds


def check_proxy_online():
    ip_url = ["http://checkip.amazonaws.com", "http://ipinfo.io/ip", "https://ifconfig.me/ip", "http://icanhazip.com/"]
    for url in ip_url:
        command = ("curl -x socks5://localhost:2080  %s" % url)
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        output, error = process.communicate()
        process.wait()
        if process.returncode == 0:
            lines = output.decode().split('\n')
            if any(is_valid_ip(line.strip()) for line in lines if line.strip()):
                return True
    return False


def is_valid_ip(ip_str):
    try:
        ipaddress.ip_address(ip_str)
        print("get_ip:%s" % ip_str)
        return True
    except ValueError:
        return False


def send(body_json, ARGs):
    body_json.update({
        's': ARGs.cnl,
        'ts': int(time.time()),
    })

    body = json.dumps(body_json)
    h = {'Authorization': hmac.new(
        bytes(ARGs.secret, 'utf8'),
        bytes(body, 'utf8'),
        hashlib.sha256).hexdigest()}
    r = requests.post(
        'https://rf.rocketfireapi.com/alive', data=body, headers=h, timeout=15)
    print(r, r.text)


def cronJobs():
    if datetime.datetime.utcnow().strftime("%H%M") == '2323':
        retlog = os.system('cp /root/ss.log /root/ss.log.tmp && echo "" > /root/ss.log')
        print("Log move %s" % retlog)

    if int(datetime.datetime.utcnow().strftime("%M")) % 2 == 0 and is_v2ray_inAlive():
        print("is_v2ray_alive: " + str(is_v2ray_inAlive()))
        os.system('reboot')


def main():
    print(datetime.datetime.utcnow())
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--cnl")
    parser.add_argument("-s", "--secret")

    ARGs, unknown = parser.parse_known_args()
    time.sleep(random.randint(0, 2))
    if not (ARGs.secret and ARGs.cnl):
        raise Exception("No arg err")

    net_stat = psutil.net_connections()
    net_io = psutil.net_io_counters()
    connect_ips = len(set([i.raddr[0] for i in net_stat if i.laddr[1] in (80, 443) and i.raddr]))
    connections = len(net_stat)
    disk = os.statvfs('/')

    status = 0
    if is_v2ray_inAlive() and not check_proxy_online():
        status = -10
    print("status:%s" % status)

    send({
        'ip': requests.get('http://ipinfo.io/ip', timeout=10).text.strip('\n'),
        'cpu': int(psutil.cpu_percent()),
        'load': os.getloadavg()[1],
        'connectips': connect_ips,
        'connections': connections,
        'tt': get_uptime(),
        'pwd': json.loads(open('/etc/ss.conf').read())['port_password']['80'],
        'freedisk': (disk.f_bavail * disk.f_frsize) // 1024 ** 2,
        'netin': net_io.bytes_recv // 1024,
        'netout': net_io.bytes_sent // 1024,
        'cores': multiprocessing.cpu_count(),
        'vip': 0,
        'status': status
    }, ARGs)

    cronJobs()


if __name__ == '__main__':
    main()